## import needed python libraries and renamed for convinience
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy as np
import math

## defined variables for normalized Gaussian distribution
mu = 0
variance = 1
sigma = math.sqrt(variance)

## created data for plot
x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)

## plot
plt.plot(x, stats.norm.pdf(x, mu, sigma))
plt.savefig("gauss.png")